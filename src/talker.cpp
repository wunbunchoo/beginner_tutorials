#include "ros/ros.h"
#include "std_msgs/Float64.h"

int main(int argc, char **argv)
{
  	ros::init(argc, argv, "talker");
  	ros::NodeHandle n;
  	ros::Publisher chatter_pub1 = n.advertise<std_msgs::Float64>("topic_x", 5);
  	ros::Publisher chatter_pub2 = n.advertise<std_msgs::Float64>("topic_y", 5);
  	ros::Rate loop_rate(10);
	float count=0.0;
while (ros::ok())
  {

	std_msgs::Float64 str1;
	std_msgs::Float64 str2;
	str1.data = count;
	str2.data = count+.05;

	chatter_pub1.publish(str1);
	chatter_pub2.publish(str2);

	ROS_INFO("%f %f", str1.data, str2.data);

    	ros::spinOnce();
    	loop_rate.sleep();

	++count;
  }

  return 0;
}
