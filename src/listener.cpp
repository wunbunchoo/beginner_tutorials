#include "ros/ros.h"
#include "std_msgs/Float64.h"
float x,y,z;

void chatterCallback1(const std_msgs::Float64::ConstPtr& msg)
{

  x = msg->data;
  
}

void chatterCallback2(const std_msgs::Float64::ConstPtr& msg)
{

  y = msg->data;
 ROS_INFO("%f %f", x, y);
  
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub1 = n.subscribe("topic_x", 1000, chatterCallback1);
  ros::Subscriber sub2 = n.subscribe("topic_y", 1000, chatterCallback2);
  
  ros::spin();

  return 0;
}
